#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

# build dosbox-staging
#
pushd "source"
./autogen.sh
./configure CXXFLAGS="-O3 -DNDEBUG" CFLAGS="-O3 -DNDEBUG"
make -j "$(nproc)"
strip src/dosbox
popd

mkdir -p "0/dosbox-staging"
cp "source/src/dosbox" "0/dosbox-staging/"
cp "source/COPYING"    "0/dosbox-staging/LICENSE"

readonly source_desc=$(git_source_describe)
readonly package_commit=$(git describe --always)
cp "README.template"                              "0/dosbox-staging/README"
sed -i "s|%GIT_COMMIT%|$package_commit|"          "0/dosbox-staging/README"
sed -i "s|%GIT_SOURCE_DESCRIPTION%|$source_desc|" "0/dosbox-staging/README"
