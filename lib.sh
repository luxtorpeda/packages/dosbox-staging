#!/bin/bash

set -e

log_environment () {
	pwd
	nproc
	gcc --version
}

git_source_describe () {
	# this version of git is old enough to miss parameter -C
	pushd source > /dev/null
	git describe "$@"
	popd > /dev/null
}

prepare_dist_dirs () {
	for app_id in $1 ; do
		mkdir -p "$app_id/dist"
		cp "README.md" "$app_id/dist/README.lux.txt"
	done
}

list_dist () {
	{
		find dist -type f
		find dist -type l
	} | sort
}
